// const Sequelize = require('sequelize')
// const Op = Sequelize.Op;
// const operatorsAliases = {
//   $in: Op.in
// }

const batchUsers = async (keys, models) => {
  const users = await models.User.findAll({
    where: {
      // id: {
      //   Op.in: keys,
      // },
      id: {
        $in: keys,
      }
    },
  });

  return keys.map(key => users.find(user => user.id === key));
};

module.exports = { batchUsers }