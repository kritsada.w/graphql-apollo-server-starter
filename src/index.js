const cors = require('cors')
const express = require('express')
const { ApolloServer, gql, AuthenticationError } = require('apollo-server-express')
const uuidv4 = require('uuid/v4')

const schema = require('./schema')
const resolvers = require('./resolvers')
const {  models, sequelize } = require('./models')

const jwt = require('jsonwebtoken');
const http = require('http');
const DataLoader = require('dataloader');
const loaders = require('./loaders');

const app = express()
app.use(cors())

const getMe = async req => {
  const token = req.headers['x-token'];

  if (token) {
    try {
      return await jwt.verify(token, process.env.SECRET);
    } catch (e) {
      throw new AuthenticationError(
        'Your session expired. Sign in again.',
      );
    }
  }
};

const batchUsers = async (keys, models) => {
  const users = await models.User.findAll({
    where: {
      id: {
        $in: keys,
      },
    },
  });

  return keys.map(key => users.find(user => user.id === key));
};

const userLoader = new DataLoader(keys => batchUsers(keys, models));

const server = new ApolloServer({
  typeDefs: schema,
  resolvers,
  formatError: error => {
    // remove the internal sequelize error message
    // leave only the important validation error
    const message = error.message
      .replace('SequelizeValidationError: ', '')
      .replace('Validation error: ', '');

    return {
      ...error,
      message,
    };
  },
  // context: async () => ({
  //   models,
  //   me: models.User.findByLogin('rwieruch'),
  //   secret: process.env.SECRET,
  // })
  // context: async ({ req }) => {
  //   const me = await getMe(req);

  //   return {
  //     models,
  //     me,
  //     secret: process.env.SECRET,
  //   };
  // }
  context: async ({ req, connection }) => {
    if (connection) {
      return {
        models,
        loaders: {
          user: new DataLoader(keys =>
            loaders.user.batchUsers(keys, models),
          ),
        },
      };
    }
    
    if (req) {
      const me = await getMe(req);

      return {
        models,
        me,
        secret: process.env.SECRET,
        // loaders: {
        //   // user: new DataLoader(keys => batchUsers(keys, models)),
        //   user: userLoader,
        // },
        loaders: {
          user: new DataLoader(keys =>
            loaders.user.batchUsers(keys, models),
          ),
        },
      };
    }
  }
})

server.applyMiddleware({ app, path: '/graphql' })

// app.listen({ port: 8000 }, () => {
//   console.log('Apollo Server on http://localhost:8000/graphql')
// })

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);


const eraseDatabaseOnSync = true;
const isTest = !!process.env.TEST_DATABASE;

// sequelize.sync({ force: eraseDatabaseOnSync }).then(async () => {
sequelize.sync({ force: isTest }).then(async () => {
  // if (eraseDatabaseOnSync) {
  if (isTest) {
    createUsersWithMessages(new Date());
  }

  // app.listen({ port: 8000 }, () => {
  //   console.log('Apollo Server on http://localhost:8000/graphql');
  // });
  httpServer.listen({ port: 8000 }, () => {
    console.log('Apollo Server on http://localhost:8000/graphql');
  });
});

const createUsersWithMessages = async (date) => {
  await models.User.create(
    {
      username: 'rwieruch',
      email: 'hello@robin.com',
      password: 'rwieruch',
      role: 'ADMIN',
      messages: [
        {
          text: 'Published the Road to learn React',
          createdAt: date.setSeconds(date.getSeconds() + 1),
        },
      ],
    },
    {
      include: [models.Message],
    },
  );

  await models.User.create(
    {
      username: 'ddavids',
      email: 'hello@david.com',
      password: 'ddavids',
      messages: [
        {
          text: 'Happy to release ...',
          createdAt: date.setSeconds(date.getSeconds() + 1),
        },
        {
          text: 'Published a complete ...',
          createdAt: date.setSeconds(date.getSeconds() + 1),
        },
      ],
    },
    {
      include: [models.Message],
    },
  );
};
