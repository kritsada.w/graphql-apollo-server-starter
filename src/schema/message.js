const { gql } = require('apollo-server-express')

const typeDefs = gql`
  extend type Query {
    # messages: [Message!]!
    # messages(offset: Int, limit: Int): [Message!]!
    # messages(cursor: String, limit: Int): [Message!]!
    messages(cursor: String, limit: Int): MessageConnection!
    message(id: ID!): Message!
  }

  extend type Mutation {
    createMessage(text: String!): Message!
    deleteMessage(id: ID!): Boolean!
  }

  type MessageConnection {
    edges: [Message!]!
    pageInfo: PageInfo!
  }

  type PageInfo {
    hasNextPage: Boolean!
    endCursor: String!
  }

  type Message {
    id: ID!
    text: String!
    userId: ID!
    createdAt: Date!
    user: User!
  }

  extend type Subscription {
    messageCreated: MessageCreated!
  }

  type MessageCreated {
    message: Message!
  }
`

module.exports = typeDefs
