const { GraphQLDateTime } = require('graphql-iso-date');

const userResolvers = require('./user.js')
const messageResolvers = require('./message')

const customScalarResolver = {
  Date: GraphQLDateTime,
}

module.exports = [customScalarResolver, userResolvers, messageResolvers]
