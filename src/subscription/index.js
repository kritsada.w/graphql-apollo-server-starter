const { PubSub } = require('apollo-server');


const MESSAGE_EVENTS = require('./message.js');

const EVENTS = {
  MESSAGE: MESSAGE_EVENTS,
};

const pubsub = new PubSub();

module.exports = {
  EVENTS,
  pubsub
}