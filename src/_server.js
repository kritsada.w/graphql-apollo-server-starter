import express from 'express'
// import { ApolloServer, gql } from 'apollo-server-express'
const { ApolloServer, gql } = require('apollo-server-express')

const app = express()

const schema = gql``
const resolvers = gql``

const typeDefs = gql``

const server = new ApolloServer({
  typeDefs: schema,
resolvers})

server.applyMiddleware({ app, path: '/graphql' })

app.listen({ port: 8000 }, () => {
  console.log('Apollo Server on http://localhost:8000/graphql')
})
